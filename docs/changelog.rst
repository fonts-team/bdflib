Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog`_,
and this project adheres to `Semantic Versioning`_.

`Unreleased`_
--------------

`v2.0.1`_ - 2022-01-14
----------------------

Fixed:

* Fixed the v2.0.0 changelog entry.

`v2.0.0`_ - 2022-01-14
----------------------

Added:

* This changelog!
* Documentation for the :attr:`.FontFiller.missing_chars`
  and :attr:`.FontFiller.unknown_classes` attributes.
* :class:`.Font` now stores the standard BDF fields as attributes
  (:attr:`~.Font.name`, :attr:`~.Font.ptSize`, :attr:`~.Font.xdpi`,
  :attr:`~.Font.ydpi`).
* :mod:`bdflib.xlfd` makes it easy to check whether a font
  conforms to the X Logical Font Descriptor conventions,
  and to automatically fix many conformance problems.

Changed:

* Now that :class:`.Font` stores BDF fields in standard attributes,
  and now that :mod:`bdflib.xlfd` exists,
  the :class:`.Font` class no longer needs to assume
  BDF custom properties are used for anything in particular.
  In particular, the standard BDF fields are no longer mirrored
  to XLFD-style properties,
  there's no special property names that cannot be overwritten,
  and XLFD properties will no longer be automatically synthesised
  when writing out a BDF font.
* The :class:`.Glyph` constructor
  (and therefore :meth:`.Font.new_glyph_from_data()`)
  now takes bitmap data in the form of a list of integers,
  not the hex-encoded string used in the BDF on-disk format.

Removed:

* Support for Python 2.x
* Support for fonts whose point-size
  (the first parameter to the `SIZE` field in the font header)
  is a non-integer. I don't know why I ever thought this was a good idea.

`v1.1.3`_ - 2019-04-23
----------------------

Fixed:

* We now support BDF comments in more places
  (although all the comments in a file are still concatenated)
* We now support values being separated from keywords
  by more than a single space.

`v1.1.2`_ - 2019-04-22
----------------------

Added:

* The examples in the API reference and tutorial
  are now automatically tested.

Fixed:

* The documentation now includes the `tutorial.bdf` file
  used by the tutorial.

`v1.1.1`_ - 2018-11-19
----------------------

Changed:

* Set the package long description to the README contents.

`v1.1.0`_ - 2018-11-19
----------------------

Added:

* Standalone documentation, including a tutorial
* The :meth:`.Glyph.iter_pixels()` method,
  making it easier to get at the glyph bitmap data.

Fixed:

* :class:`.FontFiller` now supports
  supports generating spacing characters made from only combining characters.

`v1.0.4`_ - 2016-10-19
----------------------

Added:

* Support for Python 3.

`v1.0.3`_ - 2016-09-25
----------------------

Fixed:

* No longer breaks on BDF files containing blank lines.

`v1.0.2`_ - 2016-03-27
----------------------

Added:

* a README
* Support for running tests with Tox

Fixed:

* No longer requires to be installed from a Git checkout.

`v1.0.1`_ - 2015-04-06
----------------------

Changed:

* Repository moved from Gitorious to GitLab

`v1.0.0`_ - 2009-03-22
----------------------

Initial release.

.. _Keep a Changelog: https://keepachangelog.com/en/1.0.0/
.. _Semantic Versioning: https://semver.org/spec/v2.0.0.html
.. _v1.0.0: https://gitlab.com/Screwtapello/bdflib/-/tree/v1.0.0
.. _v1.0.1: https://gitlab.com/Screwtapello/bdflib/-/compare/v1.0.0...v1.0.1
.. _v1.0.2: https://gitlab.com/Screwtapello/bdflib/-/compare/v1.0.1...v1.0.2
.. _v1.0.3: https://gitlab.com/Screwtapello/bdflib/-/compare/v1.0.2...v1.0.3
.. _v1.0.4: https://gitlab.com/Screwtapello/bdflib/-/compare/v1.0.3...v1.0.4
.. _v1.1.0: https://gitlab.com/Screwtapello/bdflib/-/compare/v1.0.4...v1.1.0
.. _v1.1.1: https://gitlab.com/Screwtapello/bdflib/-/compare/v1.1.0...v1.1.1
.. _v1.1.1: https://gitlab.com/Screwtapello/bdflib/-/compare/v1.1.0...v1.1.1
.. _v1.1.2: https://gitlab.com/Screwtapello/bdflib/-/compare/v1.1.1...v1.1.2
.. _v1.1.3: https://gitlab.com/Screwtapello/bdflib/-/compare/v1.1.2...v1.1.3
.. _v2.0.0: https://gitlab.com/Screwtapello/bdflib/-/compare/v1.1.3...v2.0.0
.. _v2.0.1: https://gitlab.com/Screwtapello/bdflib/-/compare/v2.0.0...v2.0.1
.. _Unreleased: https://gitlab.com/Screwtapello/bdflib/-/compare/v2.0.1...master
