API Reference
=============

.. automodule:: bdflib
    :members:
    :show-inheritance:

``bdflib.model``
----------------

.. automodule:: bdflib.model
    :members:
    :show-inheritance:

``bdflib.reader``
-----------------

.. automodule:: bdflib.reader
    :members:
    :show-inheritance:

``bdflib.writer``
-----------------

.. automodule:: bdflib.writer
    :members:
    :show-inheritance:

``bdflib.effects``
------------------

.. automodule:: bdflib.effects
    :members:
    :show-inheritance:

``bdflib.glyph_combining``
--------------------------

.. automodule:: bdflib.glyph_combining
    :members:
    :show-inheritance:

``bdflib.xlfd``
--------------------------

.. automodule:: bdflib.xlfd
    :members:
    :no-inherited-members:
